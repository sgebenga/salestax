package salestax.core;

import org.junit.Test;
import salestax.core.impl.ImportTaxExemptibility;
import salestax.core.impl.SalesTaxExemptibility;

import static org.junit.Assert.assertTrue;

/**
 * Created by khaya on 2015/04/26.
 */
public class TaxExemptibleTest {


    @Test
    public void shouldBeImportTaxExemptible(){
        TaxExemptible importTaxExemptible = TaxExemptibleFactory.withExemptions(ProductSource.LOCAL);
        assertTrue(importTaxExemptible instanceof TaxExemptible);
        assertTrue(importTaxExemptible instanceof ImportTaxExemptibility);
    }

    @Test
    public void shouldBeSalesTaxExemptible(){
       TaxExemptible salesTaxExemptible = TaxExemptibleFactory.withExemptions(ProductType.BOOK,ProductType.FOOD,ProductType.MEDICAL);
        assertTrue(salesTaxExemptible instanceof TaxExemptible);
        assertTrue("Should be Sales Tax Exemptible",salesTaxExemptible instanceof SalesTaxExemptibility);
    }
}
