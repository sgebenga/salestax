package salestax.core;

import org.junit.Test;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by khaya on 2015/04/26.
 */
public class ProductSourceTest {

    private static final String NAME="Import";

    @Test
    public void shouldHaveAName(){
       ProductSource productSource = ProductSource.IMPORT;
       assertNotNull(productSource);
       assertEquals(productSource.toString(),NAME);
    }
}
