package salestax.core;

import org.junit.Test;
import salestax.core.impl.TaxCalculatorImpl;

import static org.junit.Assert.assertNotNull;

/**
 * Created by khaya on 2015/04/26.
 */
public class BasketTest {

    @Test
    public void shouldCreateBasket() {
        Basket basket = Basket.newInstance(1, TaxCalculatorImpl.newInstance());
        assertNotNull(basket);

    }
}
