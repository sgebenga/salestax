package salestax.core;

import org.junit.Test;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by khaya on 2015/04/26.
 */
public class ProductTypeTest {

    private final static String NAME = "Medical";

    @Test
    public void shouldHaveAName(){
        ProductType productType = ProductType.MEDICAL;
        assertNotNull(productType);
        assertEquals(productType.toString(),NAME);

    }

}
