package salestax.core;

import org.junit.Test;
import salestax.common.Money;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by khaya on 2015/04/26.
 */
public class ProductTest {

    private static final String NAME = "Long Walk to Freedom";

    @Test
    public void shouldCreateProduct() {
        Product product = Product.newInstance(NAME, ProductType.BOOK, ProductSource.LOCAL, Money.valueOf("12.49"));
        assertNotNull(product);
        assertEquals(product.getName(), NAME);
    }
}
