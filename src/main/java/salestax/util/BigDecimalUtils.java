package salestax.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by khaya on 2015/04/26.
 */
public class BigDecimalUtils {

    private static int DEFAULT_SCALE = 2;
    private static RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP;

    public static BigDecimal withDefaultScale(BigDecimal amount) {
        return amount.setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE);
    }


}
