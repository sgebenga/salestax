package salestax.util;

import salestax.common.Money;

import java.math.BigDecimal;

/**
 * Created by khaya on 2015/04/28.
 */
public class MoneyUtils {

    public static Money round(Money money) {
        double rounded = Math.ceil(money.getAmount().doubleValue() * 20.0) / 20.0;
        return Money.valueOf(rounded);
    }
}
