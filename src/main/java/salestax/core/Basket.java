package salestax.core;

import salestax.common.Money;
import salestax.core.impl.PurchaseItem;
import salestax.core.impl.TaxItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by khaya on 2015/04/26.
 */

//TODO do we need an interface
public class Basket {
    private final int id;
    private final TaxCalculator taxCalculator;
    private final List<PurchaseItem> purchases = new ArrayList<PurchaseItem>();
    //private final List<TaxItem> taxes = new ArrayList<TaxItem>();

    private Basket(int id, TaxCalculator taxTypeCalculator) {
        this.id = id;
        this.taxCalculator = taxTypeCalculator;
    }

    public static Basket newInstance(int id, TaxCalculator taxCalculator) {
        return new Basket(id, taxCalculator);
    }

    public void add(Product product, int quantity) {
        PurchaseItem purchase = PurchaseItem.newInstance(product, product.getPrice(), quantity);
        taxCalculator.calculate(purchase);
        purchases.add(purchase);
    }

    public int getId() {
        return this.id;
    }

    public List<PurchaseItem> purchases() {
        return this.purchases;
    }


    public Money subTotal() {
        return total(purchases);
    }

    public Money taxTotal() {
        Money taxTotal = Money.ZERO;
        for (PurchaseItem purchase : purchases)
            taxTotal = taxTotal.add(purchase.getTotalTax());
        return taxTotal;
    }

    public Money total() {
        return subTotal().add(taxTotal());
    }

    private Money total(Collection<? extends Item> items) {
        Money total = Money.ZERO;
        for (Item item : items) {
            total = total.add(item.getAmount());
        }
        return total;
    }

}
