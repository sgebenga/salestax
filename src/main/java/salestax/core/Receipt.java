package salestax.core;

import salestax.common.Money;
import salestax.core.impl.PurchaseItem;
import salestax.core.impl.TaxItem;

/**
 * Created by khaya on 2015/04/26.
 */
//TODO create Receipt print : in a dedicated interface
public interface Receipt {

    void print();

}
