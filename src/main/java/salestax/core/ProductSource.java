package salestax.core;

/**
 * Created by khaya on 2015/04/26.
 */
//TODO add description
public enum ProductSource {
    LOCAL("Local"), IMPORT("Import");
    private final String name;

    ProductSource(String name){
        this.name= name;
    }

    @Override
    public String toString() {
        return name;
    }
}
