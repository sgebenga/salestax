package salestax.core;

import java.util.Arrays;
import java.util.List;

/**
 * Created by khaya on 2015/04/26.
 */
//TODO check interface is SOLID
public interface TaxExemptible {
    boolean isExemptible(Item item);
}
