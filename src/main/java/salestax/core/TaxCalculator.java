package salestax.core;

import salestax.core.impl.PurchaseItem;
import salestax.core.impl.TaxItem;

import java.util.Collection;

/**
 * Created by khaya on 2015/04/26.
 */
//TODO Look at breaking that fat interface Item into two separate smaller thinner interfaces
//TODO which is better TaxTypeProcesor or TaxCalculator
public interface TaxCalculator {

    Collection<TaxItem> calculate(PurchaseItem item);

}
