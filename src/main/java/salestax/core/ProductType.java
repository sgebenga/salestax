package salestax.core;

/**
 * Created by khaya on 2015/04/26.
 */
//TODO add description
public enum ProductType {

    BOOK("Book"), FOOD("Food"), MEDICAL("Medical"), OTHER("Other");
    private final String name;

    ProductType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}