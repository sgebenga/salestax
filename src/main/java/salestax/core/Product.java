package salestax.core;

import salestax.common.Money;

/**
 * Created by khaya on 2015/04/26.
 */
public class Product {

    private final String name;
    private final ProductType type;
    private final ProductSource source;
    private final Money price;

    private Product(String name, ProductType type, ProductSource source, Money price) {
        this.name = name;
        this.type = type;
        this.source = source;
        this.price = price;
    }

    public static Product newInstance(String name, ProductType type, ProductSource source, Money price) {
        return new Product(name, type, source, price);
    }

    public String getName() {
        return this.name;
    }

    public ProductType getType() {
        return this.type;
    }

    public ProductSource getSource() {
        return this.source;
    }

    public Money getPrice() {
        return this.price;
    }

}