package salestax.core;

import salestax.common.Money;

/**
 * Created by khaya on 2015/04/26.
 */

public interface Item {

    Money getAmount();

    Product getProduct();
}
