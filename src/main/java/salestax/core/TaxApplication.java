package salestax.core;

import salestax.common.Money;
import salestax.core.impl.ConsoleReceipt;
import salestax.core.impl.TaxCalculatorImpl;

/**
 * Created by khaya on 2015/04/26.
 */
public class TaxApplication {


    public static void main(String[] args) {

        //TODO refactor Tax to Tax and TaxCalculator to TaxCalculator
        Tax basicTax = Tax.newInstance("Sales Tax", "0.10", TaxExemptibleFactory.withExemptions(ProductType.BOOK, ProductType.FOOD, ProductType.MEDICAL));
        Tax importTax = Tax.newInstance("Import Tax", "0.05", TaxExemptibleFactory.withExemptions(ProductSource.LOCAL));

        TaxCalculatorImpl taxCalculator = TaxCalculatorImpl.newInstance();
        taxCalculator.add(basicTax);
        taxCalculator.add(importTax);

        //system setup/configuration
        Product localBook = Product.newInstance("book", ProductType.BOOK, ProductSource.LOCAL, Money.valueOf("12.49"));
        Product localCd = Product.newInstance("music CD", ProductType.OTHER, ProductSource.LOCAL, Money.valueOf("14.99"));
        Product locoChoco = Product.newInstance("chocolate bar", ProductType.FOOD, ProductSource.LOCAL, Money.valueOf("0.85"));

        //add products to Basket

        Basket one = Basket.newInstance(1, taxCalculator);
        one.add(localBook, 1);
        one.add(localCd, 1);
        one.add(locoChoco, 1);


        //create second basket
        //1 imported box of chocolates at 10.00
        //1 imported bottle of perfume at 47.50
        Product importedChoc = Product.newInstance("Imported box of Chocolates", ProductType.FOOD, ProductSource.IMPORT, Money.valueOf("10.00"));
        Product importedPerfume = Product.newInstance("Imported bottle of perfume", ProductType.OTHER, ProductSource.IMPORT, Money.valueOf("47.50"));


        //add products to second Basket

        Basket two = Basket.newInstance(2, taxCalculator);
        two.add(importedChoc, 1);
        two.add(importedPerfume, 1);

        //create third basket
        //1 imported bottle of perfume at 27.99
        //1 bottle of perfume at 18.99
        //1 packet of headache pills at 9.75
        //1 box of imported chocolates at 11.25
        Product jeanPaul = Product.newInstance("Imported bottle of perfume", ProductType.OTHER, ProductSource.IMPORT, Money.valueOf("27.99"));
        Product ingrams = Product.newInstance("Bottle of perfume", ProductType.OTHER, ProductSource.LOCAL, Money.valueOf("18.99"));
        Product grandPa = Product.newInstance("Packet of headache pills", ProductType.MEDICAL, ProductSource.LOCAL, Money.valueOf("9.75"));
        Product fererro = Product.newInstance("Imported chocolates", ProductType.FOOD, ProductSource.IMPORT, Money.valueOf("11.25"));

        Basket three = Basket.newInstance(3, taxCalculator);
        three.add(jeanPaul, 1);
        three.add(ingrams, 1);
        three.add(grandPa, 1);
        three.add(fererro, 1);


        // print receipt
        Receipt firstReceipt = ConsoleReceipt.newInstance(one, System.out);
        firstReceipt.print();
        System.out.println();
        Receipt secondReceipt = ConsoleReceipt.newInstance(two, System.out);
        secondReceipt.print();
        System.out.println();
        Receipt thirdReceipt = ConsoleReceipt.newInstance(three, System.out);
        thirdReceipt.print();

    }

}
