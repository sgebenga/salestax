package salestax.core.impl;

import salestax.common.Money;
import salestax.core.Tax;
import salestax.core.TaxCalculator;
import salestax.util.MoneyUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by khaya on 2015/04/26.
 */
public class TaxCalculatorImpl implements TaxCalculator {

    private final List<Tax> taxes = new ArrayList<Tax>();

    private TaxCalculatorImpl() {
    }

    public static TaxCalculatorImpl newInstance() {
        return new TaxCalculatorImpl();
    }

    public void add(Tax tax) {
        taxes.add(tax);
    }

    //TODO
    @Override
    public Collection<TaxItem> calculate(PurchaseItem purchaseItem) {
        List<TaxItem> taxItems = new ArrayList<TaxItem>();
        for (Tax tax : taxes) {
            if (!tax.isExemptible(purchaseItem)) {
                TaxItem taxItem = calculate(purchaseItem, tax);
                taxItems.add(taxItem);
                purchaseItem.addTax(taxItem);
            }
        }
        return taxItems;
    }

    private TaxItem calculate(PurchaseItem purchaseItem, Tax tax) {
        Money amount = MoneyUtils.round(tax.calculate(purchaseItem.getAmount()));
        return TaxItem.newInstance(purchaseItem, amount, tax);
    }

}
