package salestax.core.impl;

import salestax.core.Item;
import salestax.core.ProductSource;
import salestax.core.TaxExemptible;

import java.util.Arrays;
import java.util.List;

/**
 * Created by khaya on 2015/04/26.
 */
//TODO use generics

public class ImportTaxExemptibility implements TaxExemptible {

    private final List<ProductSource> sources;

    private ImportTaxExemptibility(ProductSource... sources) {
        this.sources = Arrays.asList(sources);
    }

    //TODO find a descriptive factory method
    public static ImportTaxExemptibility withExemptions(ProductSource... sources) {
        return new ImportTaxExemptibility(sources);
    }

    //TODO the one or out???
    @Override
    public boolean isExemptible(Item item) { //TODO create a taxable interface  for purchase item [something to distinguish the two items]
        return sources.contains(item.getProduct().getSource());
    }

}
