package salestax.core.impl;

import salestax.core.Item;
import salestax.core.ProductType;
import salestax.core.TaxExemptible;

import java.util.Arrays;
import java.util.List;

/**
 * Created by khaya on 2015/04/26.
 */

public class SalesTaxExemptibility implements TaxExemptible {

    private final List<ProductType> types;

    private SalesTaxExemptibility(ProductType... types) {
        this.types = Arrays.asList(types); // TODO change to EnumSet
    }

    public static SalesTaxExemptibility withExemptions(ProductType... types) {
        return new SalesTaxExemptibility(types);
    }

    @Override
    public boolean isExemptible(Item item) {
        return types.contains(item.getProduct().getType());
    }

}

