package salestax.core.impl;

import salestax.common.Money;
import salestax.core.Basket;
import salestax.core.Receipt;

import java.io.PrintStream;

/**
 * Created by khaya on 2015/04/26.
 */

public class ConsoleReceipt implements Receipt {

    private Basket basket;
    private final PrintStream out;


    private ConsoleReceipt(Basket basket, PrintStream out) {
        this.basket = basket;
        this.out = out;
    }

    public static ConsoleReceipt newInstance(Basket basket, PrintStream out) {
        return new ConsoleReceipt(basket, out);
    }


    public void print() {
        printHeader();
        //print purchases
        for (PurchaseItem purchase : basket.purchases()) {
            printProduct(purchase);
        }
        //print sales
        printTaxTotal(basket.taxTotal());
        printTotal(basket.total());
        printFooter();

    }

    private void printHeader() {
        out.printf("Output %d:\n", basket.getId());
    }

    private void printProduct(PurchaseItem purchase) {
        out.printf("%d %35s ----- %8s\n", purchase.getQuantity(), purchase.getProductName(), purchase.getSellingPrice());
    }

    //@Override
    private void printTaxTotal(Money taxTotal) {
        out.printf("%37s ----- %8s\n", "Sales Taxes:", taxTotal);
    }

    //@Override
    private void printTotal(Money total) {
        out.printf("%37s ----- %8s\n", "Total:", total);
    }


    private void printFooter() {
        out.println("");
    }

}
