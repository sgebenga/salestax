package salestax.core.impl;

import salestax.common.Money;
import salestax.core.Item;
import salestax.core.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by khaya on 2015/04/26.
 */

public class PurchaseItem implements Item {

    private final Product product;
    private final Money price;
    private final int quantity;
    private List<TaxItem> taxes = new ArrayList<TaxItem>();

    private PurchaseItem(Product product, Money price, int quantity) {
        this.product = product;
        this.price = price;
        this.quantity = quantity;
    }

    public static PurchaseItem newInstance(Product product, Money price, int quantity) {
        return new PurchaseItem(product, price, quantity);
    }

    public Money getAmount() {
        return price.multiply(String.valueOf(quantity));
    }

    public Product getProduct() {
        return this.product;
    }

    //TODO refactor
    public String getProductName() {
        return this.product.getName();
    }

    public int getQuantity() {
        return this.quantity;
    }

    // add tax item to purchase
    public void addTax(TaxItem taxItem) {
        this.taxes.add(taxItem);
    }

    //get total tax
    public Money getTotalTax() {
        Money totalTax = Money.ZERO;
        for (TaxItem taxItem : taxes) {
            totalTax = totalTax.add(taxItem.getAmount());
        }
        return totalTax;
    }

    //get selling price
    public Money getSellingPrice() {
        return getAmount().add(getTotalTax());
    }

}

