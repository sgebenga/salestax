package salestax.core.impl;

import salestax.common.Money;
import salestax.core.Item;
import salestax.core.Product;
import salestax.core.Tax;

import java.math.BigDecimal;

/**
 * Created by khaya on 2015/04/26.
 */
//TODO refactor use abstract class
public class TaxItem implements Item {

    private final PurchaseItem purchasedItem;
    private final Money amount;
    private final Tax taxType;

    private TaxItem(PurchaseItem purchasedItem, Money amount, Tax taxType) {
        this.purchasedItem = purchasedItem;
        this.amount = amount;
        this.taxType = taxType;
    }

    public static TaxItem newInstance(PurchaseItem purchasedItem, Money amount, Tax taxType) {
        return new TaxItem(purchasedItem, amount, taxType);
    }

    @Override
    public Money getAmount() {
        return this.amount;
    }

    @Override
    public Product getProduct() {
        return purchasedItem.getProduct();
    }

    public String getProductName() {
        return getProduct().getName();
    }

    public String getTaxName() {
        return this.taxType.getName();
    }

    public BigDecimal getTaxRate() {
        return this.taxType.getRate();
    }

}
