package salestax.core;

import salestax.common.Money;

import java.math.BigDecimal;

/**
 * Created by khaya on 2015/04/26.
 */

//TODO change to Tax or is TaxStrategy better or just Tax
public class Tax {

    private final String name;
    private final BigDecimal rate;
    private final TaxExemptible taxExemptible;

    private Tax(String name, BigDecimal rate, TaxExemptible taxApplicable) {
        this.name = name;
        this.rate = rate;
        this.taxExemptible = taxApplicable;
    }

    public static Tax newInstance(String name, String rate) {
        return new Tax(name, new BigDecimal(rate), null);
    }

    public static Tax newInstance(String name, String rate, TaxExemptible taxExemptible) {
        return new Tax(name, new BigDecimal(rate), taxExemptible);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public boolean isExemptible(Item item) {
        return this.taxExemptible == null ? Boolean.FALSE : this.taxExemptible.isExemptible(item);
    }

    public Money calculate(Money amount) {
        return amount.multiply(rate);
    }

}
