package salestax.core;

import salestax.core.impl.ImportTaxExemptibility;
import salestax.core.impl.SalesTaxExemptibility;

/**
 * Created by khaya on 2015/04/26.
 */
public class TaxExemptibleFactory {

    public static TaxExemptible withExemptions(ProductSource... sources) {
        return ImportTaxExemptibility.withExemptions(sources);
    }

    public static TaxExemptible withExemptions(ProductType... types) {
        return SalesTaxExemptibility.withExemptions(types);
    }
}
