package salestax.common;

import salestax.util.BigDecimalUtils;

import java.math.BigDecimal;

/**
 * Created by khaya on 2015/04/26.
 */
public final class Money {

    private final BigDecimal amount;

    public static Money ZERO = valueOf("0.00");

    public static Money valueOf(String amount) {
        return new Money(new BigDecimal(amount));
    }

    public static Money valueOf(double amount) {
        return new Money(new BigDecimal(amount));
    }

    private Money(BigDecimal amount) {
        this.amount = BigDecimalUtils.withDefaultScale(amount);
    }

    public BigDecimal getAmount() {
        return amount;
    }

    //TODO null checks
    public Money add(Money other) {
        return new Money(amount.add(other.amount));
    }

    //TODO null checks
    public Money multiply(BigDecimal factor) {
        return new Money(amount.multiply(factor));
    }

    public Money multiply(int factor) {
        return multiply(new BigDecimal(factor));
    }

    public Money multiply(String factor) {
        return multiply(new BigDecimal(factor));
    }


    public boolean equals(Money other) {
        return amount.equals(other.getAmount());
    }

    @Override
    public boolean equals(Object other) {
        return (other == null) ? false : (other instanceof Money && equals((Money) other));
    }


    @Override
    public int hashCode() {
        return amount.hashCode();
    }

    //TODO override equals

    @Override
    public String toString() {
        return amount.toString();
    }

}
